@servers(['web' => 'deployer@84.201.142.162'])

@setup
    $repository = 'git@gitlab.com:vladimirmart/laravel_ci.git';
    $releases_dir = '/var/www/laravel_ci/www/releases';
    $root_dir = '/var/www/laravel_ci/www';
    $app_dir = $root_dir.'/lci';
    $release = date('YmdHis');
    $new_release_dir = $releases_dir .'/'. $release;
    $new_release_dir_php = $new_release_dir . '/www/lci';
    $root_docker_dir = '/var/www/html';
    $new_docker_release_dir =  '/var/www/html/releases/'. $release . '/www/lci';
@endsetup

@story('deploy')
    clone_repository
    run_composer
    update_symlinks
@endstory

@task('clone_repository')
    echo 'Cloning repository'
    [ -d {{ $releases_dir }} ] || mkdir {{ $releases_dir }}
    git clone --depth 1 {{ $repository }} {{ $new_release_dir }}
    cd {{ $new_release_dir }}
    git reset --hard {{ $commit }}
@endtask

@task('run_composer')
    echo "Starting deployment ({{ $release }})"
    echo "Docker release dir: ({{$new_docker_release_dir}})"
    docker ps
    docker exec -i laravel_ci_php_1 sh -c "cd {{$new_docker_release_dir}} && composer install --prefer-dist --no-scripts -q -o"
@endtask

@task('update_symlinks')
    echo 'update_symlinks';
    echo 'new_docker_release_dir: {{$new_docker_release_dir}}'
    echo 'root_docker_dir: {{$root_docker_dir}}'

    echo "Linking storage directory"
    echo 'docker exec -i laravel_ci_php_1 sh -c "rm -rf {{$root_docker_dir}}/current/storage"'
    echo 'docker exec -i laravel_ci_php_1 sh -c "ln -nfs {{$root_docker_dir}}/storage {{$new_docker_release_dir}}/storage"'

    echo ''
    echo 'Linking current release'
    echo 'docker exec -i laravel_ci_php_1 sh -c "ln -nfs {{$new_docker_release_dir}} {{$root_docker_dir}}/current"'
    echo ''
    echo 'docker exec -i laravel_ci_nginx_1 sh -c "ln -nfs {{$new_docker_release_dir}} {{$root_docker_dir}}/current"'
    docker exec -i laravel_ci_php_1 sh -c "ln -nfs {{$new_docker_release_dir}} {{$root_docker_dir}}/current"
    docker exec -i laravel_ci_nginx_1 sh -c "ln -nfs {{$new_docker_release_dir}} {{$root_docker_dir}}/current"

    echo ''
    echo 'Linking .env file'
    echo 'docker exec -i laravel_ci_php_1 sh -c "ln -nfs {{$root_docker_dir}}/current/.env {{$new_docker_release_dir}}/.env"'
    docker exec -i laravel_ci_php_1 sh -c "ln -nfs {{$root_docker_dir}}/current/.env {{$new_docker_release_dir}}/.env"
@endtask


